import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div className="row">
        <nav className="navbar navbar-default">
          <div className="container-fluid">
            <div className="navbar-header">
              <a className="navbar-brand" href="index.html">
                Footer Copyright @ Rabin Tiwari
              </a>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Footer;
