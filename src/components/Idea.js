import React, { Component } from "react";

class Idea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputIdea: "",
      currentIdea: null,
      ideas: [
        {
          id: 1,
          name: "Interview with Prachanda"
        },
        {
          id: 2,
          name: "Earthquake Report 2018"
        },
        {
          id: 3,
          name: "Shrinkhala Khatiwada and Miss World 2018"
        }
      ]
    };
  }

  setTodo = event => {
    this.setState({
      inputIdea: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    // let temp = this.state.ideas;
    // temp.push({
    //   name: this.state.inputIdea
    // });
    if(this.state.inputIdea === "") {
      alert("Idea box is empty.");
      return null;
    }
    this.setState({
      ideas: [...this.state.ideas, { name: this.state.inputIdea }],
      inputIdea: ""
    });
  };

  handleUpdate = event => {
    event.preventDefault();
    let temp = this.state.ideas;
    temp[this.state.currentIdea].name = this.state.inputIdea;
    this.setState({
      ideas: temp,
      currentIdea: null,
      inputIdea: ""
    });
  };

  updateIdea = key => {
    console.log("update", key);
    this.setState({
      inputIdea: this.state.ideas[key].name,
      actionType: "update",
      currentIdea: key
    });
  };

  deleteIdea = event => {
    event.preventDefault();
    let temp = this.state.ideas;
    temp.splice(this.state.currentIdea, 1);
    this.setState({
      ideas: temp,
      currentIdea: null,
      inputIdea: ""
    });
    console.log("delete todo");
  };

  cancel = event => {
    event.preventDefault();
    this.setState({
      currentIdea: null,
      inputIdea: ""
    });
  };

  render() {
    console.log("state", this.state);
    const renderList = this.state.ideas.map((idea, index) => {
      return (
        <li
          key={index}
          onClick={() => {
            this.updateIdea(index);
          }}
          style={{ cursor: "pointer" }}
          className={
            this.state.currentIdea === index
              ? "list-group-item d-flex justify-content-between align-items-center active"
              : "list-group-item d-flex justify-content-between align-items-center "
          }
        >
          {index + 1}. {idea.name}
        </li>
      );
    });
    return (
      <div>
        <form style={{ marginTop: "20px" }}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Ideas</label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter ideas"
              onChange={this.setTodo}
              value={this.state.inputIdea}
            />
          </div>
          <button
            onClick={
              this.state.currentIdea === null
                ? this.handleSubmit
                : this.handleUpdate
            }
            type="submit"
            className="btn btn-primary"
          >
            {this.state.currentIdea === null ? "Add Ideas" : "Update Idea"}
          </button>
          {this.state.currentIdea !== null ? (
            <button
              onClick={this.deleteIdea}
              type="submit"
              className="btn btn-primary"
              style={{ marginLeft: "10px" }}
            >
              Delete Idea
            </button>
          ) : null}

          {this.state.currentIdea !== null ? (
            <button
              onClick={this.cancel}
              type="submit"
              className="btn btn-primary"
              style={{ marginLeft: "10px" }}
            >
              Cancel
            </button>
          ) : null}
        </form>
        <div className="card" style={{ width: "100%", marginTop: "20px" }}>
          <div className="card-header">News Idea List</div>
          <ul className="list-group list-group-flush">{renderList}</ul>
        </div>
      </div>
    );
  }
}
export default Idea;
