import React, { Component } from "react";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Idea from "./components/Idea";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Navbar />
        <Idea />
        <Footer />
      </div>
    );
  }
}

export default App;
